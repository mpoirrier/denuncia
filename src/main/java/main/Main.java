package main;

import cl.huerta.oop.sosafe.Servidor;
import java.io.IOException;

public class Main {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int port = Integer.parseInt(args[0]);
        try{
            Thread t = new Servidor(port);
            t.start();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
