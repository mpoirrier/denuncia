/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.huerta.oop.sosafe;

/**
 *
 * @author gohucan
 */
public enum TipoDenuncia {
    ROBO, RAYADO, MASCOTA_PERDIDA, SERVICIO_MAL_ESTADO, RUIDOS_MOLESTOS
}
