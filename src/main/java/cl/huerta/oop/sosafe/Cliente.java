/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.huerta.oop.sosafe;
import java.net.*;
import java.io.*;
import java.util.Scanner;

        
/**
 *
 * @author Maurice
 */
public class Cliente {
    
    
    public static void main(String [] args) throws IOException{
    String serverName = args[0];
    int port = Integer.parseInt(args[1]);
    
    try {
        Socket client = new Socket(serverName,port);
        
        OutputStream outToServer = client.getOutputStream();
        DataOutputStream out = new DataOutputStream(outToServer);
        out.writeByte(1);
        System.out.println("Ingresar Direccion:");
        Scanner sc = new Scanner(System.in);
        String hi = sc.nextLine();
        out.writeUTF(hi);
        out.flush();
        
        out.writeByte(2);
        System.out.println("Ingresar Descripcion:");
        hi = sc.nextLine();
        out.writeUTF(hi);
        out.flush();
        
        out.writeByte(3);
        System.out.println("Ingresar Prioridad:");
        int hi2 = sc.nextInt();
        out.writeInt(hi2);        
        out.flush();
        
        out.writeByte(4);
        System.out.println("Ingresar Tipo:");
        hi = sc.nextLine();
        out.writeUTF(hi);        
        out.flush();
        
        out.writeByte(-1);
        out.flush();
        out.close();
        
        }catch(IOException e){
        e.printStackTrace();
        }
    }
}
