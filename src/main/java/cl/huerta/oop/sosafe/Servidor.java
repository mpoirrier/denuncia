/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.huerta.oop.sosafe;

import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maurice
 */
public class Servidor extends Thread{
    private ServerSocket serverSocket;
    int cuenta = 0;
    Denuncia[] save= new Denuncia[100];
    public Servidor(int port)throws IOException{
        serverSocket = new ServerSocket(port);
        serverSocket.setSoTimeout(100000);
    }
    public void run(){
        while(true){
            try{
                Socket server = serverSocket.accept();
                DataInputStream in = new DataInputStream(server.getInputStream());
                
                boolean done = false;
                String address,descrip;
                int prio;
                TipoDenuncia tipe;
                while(!done){
                    byte messageType = in.readByte();
                    switch(messageType)
                    {
                        case 1:
                            address = in.readUTF();
                            break;
                        case 2:
                            descrip = in.readUTF();
                            break;
                        case 3:
                            prio = in.readInt();
                            break;
                        case 4:
                            String tDenuncia = in.readUTF();
                            break;
                        default:
                            done = true;
                    }
                }
                this.save[cuenta]= new Denuncia(address,descrip,prio,tipe);//Tipo denuncia falta.
                in.close();
                
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
                break;
            }
            catch(IllegalArgumentException e){
            break;
            }
        }
    }
    
}
